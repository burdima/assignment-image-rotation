#include "rotation.h"

typedef uint64_t coordinate;

struct image rotate( const struct image source) {
    coordinate x;
	coordinate y;
	struct image result = image_create(source.height, source.width);
    uint64_t pixel_amount = (source.height) * (source.width);
	struct pixel* rotated_pixels = malloc(sizeof(struct pixel) * pixel_amount);
	struct pixel* original_pixels = source.data;
	for (size_t i = 0; i < pixel_amount; i++) {
		x = (source.height)-1- i/(source.width);
		y = i % (source.width);
		rotated_pixels[y * (source.height) + x] = original_pixels[i];
	}
	result.data = rotated_pixels;
	return result;
}
