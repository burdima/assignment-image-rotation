#include "bmp.h"

uint32_t padding(uint32_t width) {     
    return 4 - ((width * sizeof(struct pixel)) % 4);
}

static enum read_status read_header(FILE* in, struct bmp_header* header) {

	if (fread(header, sizeof(struct bmp_header), 1, in) == 1)
		return READ_OK;
	else return READ_INVALID_HEADER;
}

static enum read_status read_data(FILE* in, struct image img) {
	uint32_t padding_size = padding(img.width);
	struct pixel* pixels = img.data;
	size_t width = img.width;
	size_t height = img.height;
	for (size_t i = 0; i < height; i++) {
		if (fread(pixels + width * i, sizeof(struct pixel), width, in) != width) {
			free(pixels);
			return FILE_READ_ERROR;
		}
		if (fseek(in, padding_size, SEEK_CUR) != 0) {
			free(pixels);
			return FILE_READ_ERROR;
		}
	}
	return READ_OK;
}

static enum write_status write_header(FILE* out, struct image const* img) {
	uint32_t padding_size = padding(img->width);
	struct bmp_header header = (struct bmp_header){ 0 };
	size_t image_size = sizeof(struct pixel) * img->width * img->height + padding_size * img->height;
	header.bfType = 19778;
        header.biXPelsPerMeter = 0;
	header.biYPelsPerMeter = 0;
	header.bFileSize = 54 + image_size;
	header.bOffBits = 54;
	header.biSize = 40;
	header.biWidth = img->width;
	header.biHeight = img->height;
	header.biPlanes = 1;
	header.biBitCount = 24;
	header.biCompression = 0;
	header.biSizeImage = image_size;
	header.biClrUsed = 0;
	header.biClrImportant = 0;
	if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1)
		return WRITE_ERROR;
	else return WRITE_OK;
}

static enum write_status write_content(FILE* out, struct image const img) {
	uint32_t padding_size = padding(img.width);
	size_t width = img.width;
	size_t height = img.height;
	struct pixel* current_pixel = img.data;
	for (size_t i = 0; i < height; i++) {
		if (fwrite(current_pixel, sizeof(struct pixel), width, out) != width)
			return WRITE_ERROR;
		if (fwrite(current_pixel, 1, padding_size, out) != padding_size)
			return WRITE_ERROR;
		current_pixel = current_pixel + (size_t)width;
	}
	return WRITE_OK;
}

enum write_status to_bmp(FILE* out, struct image const* img) {
	if (write_header(out, img) != WRITE_OK)
		return WRITE_ERROR;
	return write_content(out, *img);
}

enum read_status from_bmp(FILE* input, struct image* img) {
	struct bmp_header* header = malloc(sizeof(struct bmp_header));
	enum read_status header_check = read_header(input, header);
	if (header_check != READ_OK)
		return header_check;
	img->width = header->biWidth;
	img->height = header->biHeight;
	img->data = malloc(header->biWidth * header->biHeight * sizeof(struct pixel));
	enum read_status read_check = read_data(input, *img);
	free(header);
	return read_check;
}
