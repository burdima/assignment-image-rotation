#ifndef RW_FUNCTION
#define RW_FUNCTION
#include <stdio.h>
#include "image.h"
#include "bmp.h"

enum read_status get_image(char* const filename, struct image* const image);

enum write_status set_image(char* const filename, struct image* const image);

#endif
